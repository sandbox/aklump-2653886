# Drupal Module: Bootstrap Tour Library
**Author:** Aaron Klump  <sourcecode@intheloftstudios.com>

## Summary
**Provides the library files to Drupal for non-bootstrap themes.**

If you are not already using a bootstrap theme, you may not need this module.

## Installation
1. Install as usual, see [http://drupal.org/node/70151](http://drupal.org/node/70151) for further information.
1. Download an enable the Bootstrap Tour module from <https://www.drupal.org/project/bootstrap_tour>.
1. Download the the library files from: <http://bootstraptour.com/>.
1. Rename the folder and place it at `sites/all/libraries/bootstrap_tour`.
1. Now, enable this module and check the status page for correct installation.
1. If you're using GIT you might want to append the following to `sites/all/libraries/bootstrap_tour/.gitignore`.

        src
        test
        *.*
        !bootstrap-tour-standalone.min*
        !package.json

## Contact
* **In the Loft Studios**
* Aaron Klump - Developer
* PO Box 29294 Bellingham, WA 98228-1294
* _skype_: intheloftstudios
* _d.o_: aklump
* <http://www.InTheLoftStudios.com>
